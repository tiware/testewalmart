package com.mazzatec.testeWalmart;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.utils.DeviceStorage;
import com.mazzatec.testeWalmart.utils.StringUtil;

/**
 * Created by Innovative.admin on 11/06/2016.
 */
public class BroadcastNotification extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        sendNotification(context);

    }
    public void sendNotification(Context context){
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Reminder reminder = DeviceStorage.getReminderObject(context);

        Intent intent = new Intent(context,StartedActivity.class);

        PendingIntent  pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setTicker(StringUtil.NOTIFICATION_TITLE);
        builder.setContentTitle(reminder.getTitle());
        builder.setContentText(reminder.getMensage());
        builder.setSmallIcon(R.drawable.ic_wal);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_wal));
        builder.setContentIntent(pendingIntent);

        Notification notification = builder.build();
        notification.vibrate = new long[]{150, 300, 150, 600};
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        nm.notify(R.drawable.ic_wal, notification);

        try{
            Uri uriSom = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(context, uriSom);
            toque.play();
        }
        catch(Exception e){}
    }
}

