package com.mazzatec.testeWalmart.voley;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mazzatec.testeWalmart.models.Reminder;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Innovative.admin on 14/06/2016.
 */
public class PostReminder {

    private RequestQueue queue;

    private Context context;

    private Reminder reminder;

    String url = "http://192.168.56.1:8080/TesteWalmartWeb/ReminderRestService/insert";

    public  PostReminder(Context context,Reminder reminder){

        this.context = context;

        this.reminder = reminder;

        queue = VolleySingleton.getInstance(context).getRequestQueue();

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(

                Request.Method.POST,

                url,

                getParamsEnviar(),

                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject resp) {

                        Log.e("korea","................response ok");

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("korea","................onErrorResponse"+error.getCause());
            }

        });

        jsonObjRequest.setRetryPolicy(VolleyTimeout.recuperarTimeout());

        queue.add(jsonObjRequest);

    }

    private JSONObject getParamsEnviar() {

        JSONObject params = new JSONObject();

        try {

            params.put("id", 0);

            params.put("timeRepeat", reminder.getTimeRepeat());

            params.put("title",reminder.getTitle());

            params.put("mensage", reminder.getMensage());

            params.put("date", reminder.getDate());

            params.put("dateInsert", reminder.getDateInsert());

            params.put("noticeBefore",reminder.getNoticeBefore());

            params.put("cancelado", reminder.isCancelado());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return params;

    }


}
