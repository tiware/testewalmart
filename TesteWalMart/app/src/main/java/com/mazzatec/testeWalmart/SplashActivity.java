package com.mazzatec.testeWalmart;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        new android.os.Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = null;

                intent = new Intent(SplashActivity.this, LoginActivity.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(intent);

                finish();
            }
        }, SPLASH_TIME_OUT);

    }

}
