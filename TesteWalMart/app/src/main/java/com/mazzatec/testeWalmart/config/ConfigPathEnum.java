package com.mazzatec.testeWalmart.config;

import android.os.Environment;

import java.io.File;

/**
 * Createdby Richard on 22/10/15.
 */
public enum ConfigPathEnum {

    PATH_SDCARD_AVATAR(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Mazzatech/Walmart/Avatar").getPath());

    private String valor;

    private ConfigPathEnum(String valor){
        this.valor= valor;
    }

    public String getValor(){
        return valor;
    }

}
