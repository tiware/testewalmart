package com.mazzatec.testeWalmart;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class HistoricActivity extends BaseMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        super.setupToolbar(HistoricActivity.this);
        super.setupNavigation(HistoricActivity.this);
    }
}
