package com.mazzatec.testeWalmart.dao;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.mazzatec.testeWalmart.MainActivity;
import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.models.User;
import com.mazzatec.testeWalmart.utils.AlarmUtil;
import com.mazzatec.testeWalmart.utils.DeviceStorage;
import com.mazzatec.testeWalmart.utils.InsertNextAlarm;
import com.mazzatec.testeWalmart.utils.StringUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Innovative.admin on 11/06/2016.
 */
public class ReminderDao {


    private Context _context;

    private Dao<Reminder,Integer> getDao() throws SQLException {

        DatabaseHelper helper = MasterDAO.iniciarDataBase(_context);
        Dao<Reminder,Integer> tpdao = helper.getReminderDao();
        return tpdao;
    }

    public ReminderDao(Context contexto){this._context = contexto;}

    public List<Reminder> getAll() {

        List<Reminder> returningTipos;

        try {

            Dao<Reminder,Integer> tpdao = getDao();
            QueryBuilder<Reminder, Integer> orderQb = tpdao.queryBuilder();
            orderQb.orderBy("date",true).where().eq("cancelado",false);

            returningTipos = orderQb.query();

        } catch (Exception e) {

            e.printStackTrace();
            returningTipos = null;
        }
        return returningTipos;
    }

    public Reminder getReminder() {

        List<Reminder> returningTipos;

        try {

            returningTipos = getAll();

        } catch (Exception e) {

            e.printStackTrace();
            returningTipos = null;
        }

        if(returningTipos != null && returningTipos.size() > 0){
            return returningTipos.get(0);
        }else{

            return null;
        }
    }

    public boolean insert(Reminder registroParaInserir) {

        if(registroParaInserir.getId() > 0){
           return update(registroParaInserir);
        }else{

            boolean returningStatus;

            try {
                Dao<Reminder,Integer> tpdao =  getDao();
                tpdao.create(registroParaInserir);
                returningStatus = true;
                updateAlarm();
            } catch (SQLException e) {

                e.printStackTrace();
                returningStatus = false;
            }
            return returningStatus;

        }

    }

    public boolean update(Reminder registroParaInserir) {

        boolean returningStatus;

        try {
            Dao<Reminder,Integer> tpdao =  getDao();

            tpdao.update(registroParaInserir);

            returningStatus = true;
            updateAlarm();
        } catch (Exception e) {

            e.printStackTrace();
            returningStatus = false;
        }
        return returningStatus;
    }

    public boolean delete(Reminder registroParaDeletar){

        boolean returningStatus;

        try{

            Dao<Reminder,Integer> tpdao =  getDao();
            tpdao.delete(registroParaDeletar);
            returningStatus = true;
            updateAlarm();

        }catch (Exception e){
            e.printStackTrace();
            returningStatus = false;
        }

        return  returningStatus;

    }

    private void updateAlarm(){

        finalize();
        new InsertNextAlarm(_context,null);
    }

    @Override
    public void finalize(){
        MasterDAO.fecharDatabase();
    }

}
