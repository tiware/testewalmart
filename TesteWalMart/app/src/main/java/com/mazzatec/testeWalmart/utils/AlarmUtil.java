package com.mazzatec.testeWalmart.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mazzatec.testeWalmart.dao.ReminderDao;
import com.mazzatec.testeWalmart.models.Reminder;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Innovative.admin on 13/06/2016.
 */
public class AlarmUtil {

    private Context context;

    public  AlarmUtil(Context context){
        this.context = context;

        //boolean notification = (PendingIntent.getBroadcast(context, 0, new Intent(StringUtil.NOTIFICATION_STARTED), PendingIntent.FLAG_NO_CREATE) == null);
        //if(notification){

            Reminder reminder = DeviceStorage.getReminderObject(context);

            if(reminder != null){

                Intent intent = new Intent(StringUtil.NOTIFICATION_STARTED);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                Date now = new Date();
                long miliNow = now.getTime();

                long miliAlarm = reminder.getDate().getTime();

                long miliToAlarm = miliAlarm - miliNow;
                miliToAlarm = miliToAlarm - reminder.getNoticeBefore();

                int timeRest = (int) (miliToAlarm/1000);
                Log.e("korea","......segundos"+timeRest);

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(System.currentTimeMillis());
                c.add(Calendar.SECOND, timeRest);

                AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarme.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), reminder.getTimeRepeat(), pendingIntent);

            }else{
                Intent intent = new Intent(StringUtil.NOTIFICATION_STARTED);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarme.cancel(pendingIntent);
            }


        //}
    }
}
