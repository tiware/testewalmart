package com.mazzatec.testeWalmart.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Innovative.admin on 11/06/2016.
 */

@DatabaseTable(tableName = "Reminder")
public class Reminder implements Serializable{

    @DatabaseField(columnName = "id",generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.LONG,columnName = "timeRepeat")
    private long timeRepeat;

    @DatabaseField(canBeNull = true, width = 50, columnName = "title")
    private String title;

    @DatabaseField(canBeNull = true, width = 50, columnName = "mensage")
    private String mensage;

    @DatabaseField(canBeNull = true,dataType = DataType.DATE_STRING,format = "yyyy/MM/dd HH:mm",columnName = "date")
    private Date date;

    @DatabaseField(canBeNull = true,dataType = DataType.DATE_STRING,format = "yyyy/MM/dd HH:mm",columnName = "dateInsert")
    private Date dateInsert;

    @DatabaseField(dataType = DataType.LONG,columnName = "noticeBefore")
    private long noticeBefore;

    @DatabaseField(dataType =  DataType.BOOLEAN,columnName = "cancelado")
    private boolean cancelado;

    public Reminder(String title,String mensage,long timeRepeat,Date dateInsert,Date date,long noticeBefore){

        this.title = title;
        this.mensage = mensage;
        this.timeRepeat = timeRepeat;
        this.date = date;
        this.dateInsert = dateInsert;
        this.noticeBefore = noticeBefore;
    }

    public Reminder(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTimeRepeat() {
        return timeRepeat;
    }

    public void setTimeRepeat(long timeRepeat) {
        this.timeRepeat = timeRepeat;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMensage() {
        return mensage;
    }

    public void setMensage(String mensage) {
        this.mensage = mensage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateInsert() {
        return dateInsert;
    }

    public void setDateInsert(Date dateInsert) {
        this.dateInsert = dateInsert;
    }

    public long getNoticeBefore() {
        return noticeBefore;
    }

    public void setNoticeBefore(long noticeBefore) {
        this.noticeBefore = noticeBefore;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }
}
