package com.mazzatec.testeWalmart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mazzatec.testeWalmart.models.User;
import com.mazzatec.testeWalmart.utils.DeviceStorage;

public class LoginActivity extends AppCompatActivity {

    private EditText editLoginPassword;

    private EditText editLoginUser;

    private RelativeLayout relativeLogin;

    TextView textLoginEsqueciSenha;

    private Context context;

    String mockSenha = "w";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;



        relativeLogin = (RelativeLayout) findViewById(R.id.layLogin);

        editLoginPassword = (EditText) findViewById(R.id.editLoginPassword);
        editLoginUser = (EditText) findViewById(R.id.editLoginUser);

        editLoginPassword.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    entrar();
                }


                return false;
            }
        });


        textLoginEsqueciSenha = (TextView) findViewById(R.id.textLoginEsqueciSenha);
        textLoginEsqueciSenha.setMovementMethod(LinkMovementMethod.getInstance());
        textLoginEsqueciSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "Recuperar Senha\n usuario e Senha = w", Toast.LENGTH_LONG).show();
            }
        });

        Button btnLoginEnter = (Button) findViewById(R.id.btnLoginEnter);
        if (btnLoginEnter != null) {
            btnLoginEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    entrar();
                }
            });
        }
    }

    private void entrar() {

        if (editLoginUser.getText().toString().equals("")) {

            Snackbar.make(relativeLogin, context.getString(R.string.user_requerid), Snackbar.LENGTH_LONG).show();
            return;

        } else if (editLoginPassword.getText().toString().equals("")) {

            Snackbar.make(relativeLogin, context.getString(R.string.password_requerid), Snackbar.LENGTH_LONG).show();
            return;
        }

        if(!editLoginPassword.getText().toString().equals(mockSenha) || !editLoginUser.getText().toString().equals(mockSenha)){
            Snackbar.make(relativeLogin, "Usuário ou senha incorretos", Snackbar.LENGTH_LONG).show();
            return;
        }

        User user = new User();
        user.setId(99);
        user.setEmail("teste@email.com.br");
        user.setNome("Usuario Teste");

        DeviceStorage.setUserObject(user,context);

        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();

    }
}