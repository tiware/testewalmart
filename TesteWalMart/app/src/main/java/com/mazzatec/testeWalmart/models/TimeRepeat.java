package com.mazzatec.testeWalmart.models;

/**
 * Created by Innovative.admin on 12/06/2016.
 */
public class TimeRepeat {

    private long timeInMilics;

    private String description;

    public long getTimeInMilics() {
        return timeInMilics;
    }

    public void setTimeInMilics(long timeInMilics) {
        this.timeInMilics = timeInMilics;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
