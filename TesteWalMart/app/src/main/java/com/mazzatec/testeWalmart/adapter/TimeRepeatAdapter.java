package com.mazzatec.testeWalmart.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mazzatec.testeWalmart.R;
import com.mazzatec.testeWalmart.models.TimeRepeat;

import java.util.List;

/**
 * Created by Innovative.admin on 12/06/2016.
 */
public class TimeRepeatAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;

    private List<TimeRepeat> values;

    private ViewHolder holder;

    public TimeRepeatAdapter(Context context, List<TimeRepeat> values){

        this.context = context;
        this.values = values;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return values.get(position).getTimeInMilics();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final TimeRepeat eval = values.get(position);

        if(convertView == null){

            convertView = inflater.inflate(R.layout.reminder_item_two, null);

            holder = new ViewHolder();
            holder.text_mensage= (TextView)convertView.findViewById(R.id.text_mensage);

            convertView.setTag(holder);


        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text_mensage.setText(eval.getDescription());
        return convertView;
    }

    static class ViewHolder {
        TextView text_mensage;
    }
}
