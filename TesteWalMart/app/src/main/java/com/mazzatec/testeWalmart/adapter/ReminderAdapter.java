package com.mazzatec.testeWalmart.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mazzatec.testeWalmart.R;
import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.utils.DateStringUtil;

import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Innovative.admin on 12/06/2016.
 */
public class ReminderAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private LayoutInflater inflater;

    private Filter prospectFilter;

    private List<Reminder> reminderList;
    private List<Reminder> reminderListTemp;

    private ViewHolder holder;

    private boolean gridTwo;

    public ReminderAdapter(Context context,List<Reminder> reminderList,boolean gridTwo){

        this.context = context;
        this.reminderList = reminderList;
        this.gridTwo = gridTwo;
        this.reminderListTemp = reminderList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reminderList.size();
    }

    @Override
    public Object getItem(int position) {
        return reminderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return reminderList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Reminder reminder = reminderList.get(position);

        if(convertView == null){

            if(gridTwo){
                convertView = inflater.inflate(R.layout.reminder_item, null);
            }else {
                convertView = inflater.inflate(R.layout.reminder_item, null);
            }

            holder = new ViewHolder();
            holder.text_mensage= (TextView)convertView.findViewById(R.id.text_mensage);
            holder.text_date= (TextView)convertView.findViewById(R.id.text_date);

            convertView.setTag(holder);


        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text_mensage.setText(reminder.getTitle());

        if(reminder.getDate() != null){
            holder.text_date.setText(DateStringUtil.datetoString(reminder.getDate()));
        }else{
            holder.text_date.setText("");
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (prospectFilter == null)
            prospectFilter = new ProspectFilter();

        return prospectFilter;
    }

    public void resetData() {
        reminderList = reminderListTemp;
    }

    private class ProspectFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {

                results.values = reminderListTemp;
                results.count = reminderListTemp.size();
            } else {

                List<Reminder> nProspect = new ArrayList<Reminder>();

                for (Reminder p : reminderList) {

                    if (p.getTitle()!= null &&  ((String) removerAcentos(p.getTitle())).toUpperCase()
                            .contains(removerAcentos(constraint.toString().toUpperCase()))) {

                        nProspect.add(p);

                    }
                }

                results.values = nProspect;
                results.count = nProspect.size();

            }
            return results;
        }

        public CharSequence removerAcentos(CharSequence str) {
            return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

/*			if (results.count == 0)
				notifyDataSetInvalidated();
			else {*/
            reminderList = (List<Reminder>) results.values;
            notifyDataSetChanged();
            //}

        }

    }


    static class ViewHolder {
        TextView text_mensage,text_date;
    }
}
