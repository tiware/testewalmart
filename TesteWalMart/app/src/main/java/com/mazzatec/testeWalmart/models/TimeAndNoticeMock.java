package com.mazzatec.testeWalmart.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Innovative.admin on 12/06/2016.
 */
public class TimeAndNoticeMock {


    String jsonInStringTimeRepeat = "[{\"timeInMilics\":120000,\"description\":\"Repetir a cada 2 Minutos\"}," +
            "{\"timeInMilics\":300000,\"description\":\"Repetir a cada 5 Minutos\"}," +
            "{\"timeInMilics\":600000,\"description\":\"Repetir a cada 10 Minutos\"}," +
            "{\"timeInMilics\":180000,\"description\":\"Repetir a cada 15 Minutos\"}]";

    String jsonInStringNoticeBefore = "[{\"timeInMilics\":120000,\"description\":\"Lembrar 2 Minutos antes\"}," +
            "{\"timeInMilics\":300000,\"description\":\"Lembrar 5 Minutos antes\"}," +
            "{\"timeInMilics\":600000,\"description\":\"Lembrar 10 Minutos antes\"}," +
            "{\"timeInMilics\":180000,\"description\":\"Lembrar 30 Minutos antes\"}," +
            "{\"timeInMilics\":360000,\"description\":\"Lembrar 1 Hora antes\"}," +
            "{\"timeInMilics\":8640000,\"description\":\"Lembrar 1 Dia antes\"}]";

    public List<TimeRepeat> getTimeRepeatList(){

        try{

            Gson gson = new Gson();
            Type collectionType = new TypeToken<List<TimeRepeat>>() {}.getType();
            List<TimeRepeat> list = gson.fromJson(jsonInStringTimeRepeat, collectionType);

            return  list;

        }catch (Exception e){
            return  new ArrayList<>();
        }


    }

    public List<NoticeBefore> getNOticeBefore(){

        try{

            Gson gson = new Gson();
            Type collectionType = new TypeToken<List<NoticeBefore>>() {}.getType();
            List<NoticeBefore> list = gson.fromJson(jsonInStringNoticeBefore, collectionType);

            return  list;

        }catch (Exception e){
            return  new ArrayList<>();
        }


    }

    public String getTimerBefore(long milisecs){

        List<NoticeBefore> list = getNOticeBefore();

        for (NoticeBefore nb:list) {

            if(nb.getTimeInMilics() == milisecs){

                return nb.getDescription();

            }
        }
        return "";
    }

}
