package com.mazzatec.testeWalmart;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mazzatec.testeWalmart.adapter.NoticeBeforeAdapter;
import com.mazzatec.testeWalmart.adapter.TimeRepeatAdapter;
import com.mazzatec.testeWalmart.dao.ReminderDao;
import com.mazzatec.testeWalmart.models.NoticeBefore;
import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.models.TimeAndNoticeMock;
import com.mazzatec.testeWalmart.models.TimeRepeat;
import com.mazzatec.testeWalmart.utils.DateStringUtil;
import com.mazzatec.testeWalmart.utils.StringUtil;
import com.mazzatec.testeWalmart.voley.PostHttp;
import com.mazzatec.testeWalmart.voley.PostReminder;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewRemminderActivity extends BaseMenuActivity {

    private List<NoticeBefore> noticeBeforeList;
    private List<TimeRepeat> timeRepeatList;
    private Date dateReminder;

    private EditText editTitle;
    private TextView textDateReminder;
    private Spinner spnNoticeBefore;
    private Spinner spnTimeRepeat;
    private EditText editMensage;
    private Button btn_saveReminder;
    private RelativeLayout layoutNewReminder;
    private ImageView imgClock;

    private Context context;
    private Dialog dialog;

    private DatePicker calendarView;
    private TimePicker pickerView;

    private TimeRepeatAdapter timeRepeatAdapter;
    private NoticeBeforeAdapter noticeBeforeAdapter;

    private NoticeBefore noticeBefore;
    private TimeRepeat timeRepeat;

    private Reminder reminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_remminder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        context =  this;

        try {
            Bundle b = getIntent().getExtras();
            reminder = (Reminder) b.getSerializable(StringUtil.CURRENT_REMINDER);
        }catch (Exception e){
            e.printStackTrace();
            reminder = new Reminder();
        }


        super.setupToolbar(NewRemminderActivity.this);
        super.setupNavigation(NewRemminderActivity.this);

        layoutNewReminder = (RelativeLayout) findViewById(R.id.layoutNewReminder);
        editTitle = (EditText) findViewById(R.id.editTitle);
        textDateReminder = (TextView) findViewById(R.id.textDateReminder);
        spnNoticeBefore = (Spinner) findViewById(R.id.spnNoticeBefore);
        spnTimeRepeat = (Spinner) findViewById(R.id.spnTimeRepeat);
        editMensage = (EditText) findViewById(R.id.editMensage);
        btn_saveReminder = (Button) findViewById(R.id.btn_saveReminder);
        imgClock = (ImageView) findViewById(R.id.imgClock);

        btn_saveReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTitle.getText().toString().equals("")) {

                    Snackbar.make(layoutNewReminder, "Adicione um título ao seu lembrete", Snackbar.LENGTH_LONG).show();
                    return;
                } else if (dateReminder == null) {
                    Snackbar.make(layoutNewReminder, "Selecione uma data para seu lembrete", Snackbar.LENGTH_LONG).show();
                    return;
                } else if (!validateDate()) {
                    Snackbar.make(layoutNewReminder, "A data informada n[ao é valida", Snackbar.LENGTH_LONG).show();
                    return;
                }
                //reminder = new Reminder(editTitle.getText().toString(),editMensage.getText().toString(),timeRepeat.getTimeInMilics(),new Date(),dateReminder,noticeBefore.getTimeInMilics());
                reminder.setTitle(editTitle.getText().toString());
                reminder.setDate(dateReminder);
                reminder.setDateInsert(new Date());
                reminder.setCancelado(false);
                reminder.setMensage(editMensage.getText().toString());
                reminder.setNoticeBefore(noticeBefore.getTimeInMilics());
                reminder.setTimeRepeat(timeRepeat.getTimeInMilics());
                ReminderDao reminderDao = new ReminderDao(context);
                boolean ins = reminderDao.insert(reminder);

                //new PostReminder(context,reminder);

                if (ins) {
                    Snackbar.make(layoutNewReminder, "Lembrete inserido com sucesso", Snackbar.LENGTH_LONG).show();
                    dateReminder = null;
                    updateDateLabel();
                    editTitle.setText("");
                    editMensage.setText("");
                    reminder = new Reminder();
                } else {
                    Snackbar.make(layoutNewReminder, "Erro ao inserir lembrete, tente novamente", Snackbar.LENGTH_LONG).show();
                }
            }
        });
        imgClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectDate();

            }
        });
        textDateReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectDate();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        TimeAndNoticeMock timeAndNoticeMock  = new TimeAndNoticeMock();
        noticeBeforeList = timeAndNoticeMock.getNOticeBefore();
        timeRepeatList = timeAndNoticeMock.getTimeRepeatList();

        if(noticeBeforeList != null && noticeBeforeList.size() > 0){
            noticeBefore = noticeBeforeList.get(0);
        }

        if(timeRepeatList != null && timeRepeatList.size() > 0){
            timeRepeat = timeRepeatList.get(0);
        }

        timeRepeatAdapter = new TimeRepeatAdapter(context,timeRepeatList);
        noticeBeforeAdapter = new NoticeBeforeAdapter(context,noticeBeforeList);

        spnNoticeBefore.setAdapter(noticeBeforeAdapter);
        spnTimeRepeat.setAdapter(timeRepeatAdapter);

        spnNoticeBefore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                noticeBefore = noticeBeforeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTimeRepeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                timeRepeat = timeRepeatList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if(reminder.getId() > 0){

            for (int i = 0; i < noticeBeforeList.size();i++){
                if(noticeBeforeList.get(i).getTimeInMilics() == reminder.getNoticeBefore()){
                    spnNoticeBefore.setSelection(i);
                    break;
                }
            }

            for (int i = 0; i < timeRepeatList.size();i++){
                if(timeRepeatList.get(i).getTimeInMilics() == reminder.getTimeRepeat()){
                    spnTimeRepeat.setSelection(i);
                    break;
                }
            }

            dateReminder = reminder.getDate();
            editTitle.setText(reminder.getTitle());
            textDateReminder.setText(DateStringUtil.datetoString(reminder.getDate()));
            editMensage.setText(reminder.getMensage());
        }


    }

    public void selectDate(){

        final int[] timeArray = new int[5];

        final Calendar cal = Calendar.getInstance();
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.item_date, null);

        calendarView = (DatePicker) linearLayout.findViewById(R.id.calendarView);
        calendarView.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        pickerView = (TimePicker) linearLayout.findViewById(R.id.pickerView);
        pickerView.setIs24HourView(true);

        timeArray[3] = cal.get(Calendar.HOUR_OF_DAY);
        timeArray[4] = cal.get(Calendar.MINUTE);

        pickerView.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                timeArray[3] = hourOfDay;
                timeArray[4] = minute;
            }
        });

        Button btnvoltar = (Button) linearLayout.findViewById(R.id.btnvoltar);
        btnvoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog = null;
            }
        });

        Button btnSelect = (Button) linearLayout.findViewById(R.id.btnSelect);
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timeArray[0] = calendarView.getYear();
                timeArray[1] = calendarView.getMonth()+1;
                timeArray[2] = calendarView.getDayOfMonth();

                if(validateDate(timeArray)){

                    updateDateLabel();
                    dialog.dismiss();
                    dialog = null;

                }else{
                    Toast.makeText(context,"Data informada inválida",Toast.LENGTH_LONG).show();
                }
            }
        });


        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(linearLayout);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    private boolean validateDate(int[] timeArray){

        Date dt = new Date();
        String dateString =timeArray[0]+"/"+timeArray[1]+"/"+timeArray[2]+" "+timeArray[3]+":"+timeArray[4];
        Date dtReminder = null;
        try {
            dtReminder = DateStringUtil.stringToDate(dateString);

        } catch (ParseException e) {
            e.printStackTrace();
            dtReminder =dt;
        }

        int compared = dtReminder.compareTo(dt);

        boolean dateValid = false;
        if(compared > -1){
            dateValid = true;
            dateReminder = dtReminder;
        }else{
            updateDateLabel();
        }

        return dateValid;

    }

    private boolean validateDate(){

        if(dateReminder == null){
            return false;
        }

        Date dt = new Date();

        int compared = dateReminder.compareTo(dt);

        boolean dateValid = false;
        if(compared > -1){
            return true;
        }
        return dateValid;

    }
    private void updateDateLabel(){

        if(dateReminder != null){
            textDateReminder.setText(DateStringUtil.datetoString(dateReminder));
        }else{
            textDateReminder.setText("Data do lembrete");
        }

    }

}
