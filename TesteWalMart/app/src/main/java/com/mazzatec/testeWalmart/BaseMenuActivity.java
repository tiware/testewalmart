package com.mazzatec.testeWalmart;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mazzatec.testeWalmart.config.ConfigPathEnum;
import com.mazzatec.testeWalmart.models.User;
import com.mazzatec.testeWalmart.utils.DeviceStorage;
import com.mazzatec.testeWalmart.utils.StringUtil;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class BaseMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Bitmap _bitmap = null;
    private View header;
    private android.support.v7.app.AppCompatActivity _activity;
    private android.support.v7.widget.Toolbar _toolbar;

    private User user;

    private Context context;

    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

    }

    protected Toolbar setupToolbar(final android.support.v7.app.AppCompatActivity activity) {

        this._activity = activity;

        _toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(_toolbar);
        //_toolbar.setLogo(R.drawable.vivara);
        return _toolbar;
    }

    protected void setupNavigation(final android.support.v7.app.AppCompatActivity activity) {
        setupNavigation(activity, _toolbar);
    }

    protected NavigationView setupNavigation(final android.support.v7.app.AppCompatActivity activity, Toolbar toolbar) {

        this._activity = activity;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        header=navigationView.getHeaderView(0);

        CircularImageView imageHeaderAvatar = (CircularImageView) header.findViewById(R.id.imgUser);
        imageHeaderAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
                galleryIntent.setType("image/*");
                galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);


                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
                chooser.putExtra(Intent.EXTRA_TITLE, "Selecione local para adicionar imagem");

                Intent[] intentArray = {cameraIntent};
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooser, StringUtil.SELECT_PICTURE);

            }
        });

        user = DeviceStorage.getUserObject(BaseMenuActivity.this);
        if(user != null){
            TextView textViewNameUser = (TextView)header.findViewById(R.id.textViewNameUser);
            TextView textViewEmailUser = (TextView)header.findViewById(R.id.textViewEmailUser);
            textViewNameUser.setText(user.getNome());
            textViewEmailUser.setText(user.getEmail());

        }

        updateNaveBar(user);

        return navigationView;

    }

    public void updateNaveBar(User newUser){
        if(newUser != null){
            TextView textViewNameUser = (TextView)header.findViewById(R.id.textViewNameUser);
            TextView textViewEmailUser = (TextView)header.findViewById(R.id.textViewEmailUser);
            textViewNameUser.setText(newUser.getNome());
            textViewEmailUser.setText(newUser.getEmail());

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setAvatarImage();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            sairApp();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_myReminder) {

            Intent intent = new Intent(context,HomeActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_newReminder) {

            Intent intent = new Intent(context,NewRemminderActivity.class);
            startActivity(intent);
            finish();

        }else if (id == R.id.nav_historic) {

            //Intent intent = new Intent(context,HistoricActivity.class);
            //startActivity(intent);
            //finish();
            Toast.makeText(context,"Falta implementar",Toast.LENGTH_LONG).show();

        }else if (id == R.id.nav_account) {

            //Intent intent = new Intent(context,AccountActivity.class);
            //startActivity(intent);
            //finish();
            Toast.makeText(context,"Falta implementar",Toast.LENGTH_LONG).show();

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAvatarImage() {

        CircularImageView circularImageView = (CircularImageView) header.findViewById(R.id.imgUser);
        circularImageView.setBorderColor(getResources().getColor(R.color.colorHeader));
        circularImageView.setBorderWidth(1);
        //circularImageView.addShadow();

        try{

            String fileName = ConfigPathEnum.PATH_SDCARD_AVATAR.getValor() + "/" +user.getId()+".png";
            File file = new File(fileName);

            if (file.exists()) {

                try{

                    //Glide.with(MinhaContaActivity.this).load(new File(fileName)).into(circularImageView);
                    Glide.with(context).load(new File(fileName)).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(circularImageView);
                    //Bitmap bitmapToSet = BitmapFactory.decodeFile(file.getAbsolutePath());
                    //circularImageView.setImageBitmap(bitmapToSet);

                }catch (Exception e){

                    Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_user);
                    circularImageView.setImageBitmap(bm);
                    e.printStackTrace();
                }


            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == StringUtil.SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
            if (data.getData() != null) {
                try {
                    if (_bitmap != null) {
                        _bitmap.recycle();
                    }
                    InputStream stream = getContentResolver().openInputStream(data.getData());
                    _bitmap = BitmapFactory.decodeStream(stream);
                    saveBitmap(_bitmap);
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                _bitmap = (Bitmap) data.getExtras().get("data");
                saveBitmap(_bitmap);
            }

            setAvatarImage();
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void saveBitmap(Bitmap bitmap) {
        String file_path = ConfigPathEnum.PATH_SDCARD_AVATAR.getValor();

        File dir = new File(file_path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(file_path, "/" +user.getId()+ ".png");
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
    }

    private void sairApp() {

        LayoutInflater inflater = (LayoutInflater) BaseMenuActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.item_sair, null);

        Button btnAvancar = (Button) linearLayout.findViewById(R.id.btn_itemSair_avancar);

        btnAvancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                dialog = null;

                //BaseMenuActivity.this.finish();
                //System.exit(0);
                //MasterDAO.fecharDatabase();
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());


            }
        });

        Button btnCancelar = (Button) linearLayout.findViewById(R.id.btn_itemSair_cancelar);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                dialog = null;

            }
        });

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(linearLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }
}

