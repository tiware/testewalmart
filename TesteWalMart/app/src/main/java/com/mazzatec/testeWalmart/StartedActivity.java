package com.mazzatec.testeWalmart;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.utils.DeviceStorage;
import com.mazzatec.testeWalmart.utils.InsertNextAlarm;
import com.mazzatec.testeWalmart.utils.StringUtil;

public class StartedActivity extends BaseMenuActivity {

    private Reminder reminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_started);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        reminder = DeviceStorage.getReminderObject(StartedActivity.this);

        TextView textStartedDesc = (TextView) findViewById(R.id.textStartedDesc);
        TextView textStartedTitle = (TextView) findViewById(R.id.textStartedTitle);

        textStartedTitle.setText(reminder.getTitle());
        textStartedDesc.setText(reminder.getMensage());


        Button btnStartedStop = (Button) findViewById(R.id.btnStartedStop);
        btnStartedStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopAlarm(reminder);
            }
        });

        super.setupToolbar(StartedActivity.this);
        super.setupNavigation(StartedActivity.this);
    }

    private void stopAlarm(Reminder reminder){        


        new InsertNextAlarm(StartedActivity.this,reminder);

        finish();

    }
}
