package com.mazzatec.testeWalmart.dao;

import android.app.Activity;
import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.mazzatec.testeWalmart.models.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Innovative.admin on 11/06/2016.
 */
public class UserDao {


    private Context _context;

    private Dao<User,Integer> getDao() throws SQLException {

        DatabaseHelper helper = MasterDAO.iniciarDataBase(_context);
        Dao<User,Integer> tpdao = helper.getUserDao();
        return tpdao;
    }

    public UserDao(Context contexto){this._context = contexto;}

    public List<User> getAll() {

        List<User> returningTipos;

        try {

            Dao<User,Integer> tpdao = getDao();
            returningTipos = tpdao.queryForAll();

        } catch (Exception e) {

            e.printStackTrace();
            returningTipos = null;
        }
        return returningTipos;
    }

    public User getUser() {

        List<User> returningTipos;

        try {

            Dao<User,Integer> tpdao = getDao();
            returningTipos = tpdao.queryForAll();

        } catch (Exception e) {

            e.printStackTrace();
            returningTipos = null;
        }

        if(returningTipos != null && returningTipos.size() > 0){
            return returningTipos.get(returningTipos.size() - 1);
        }else{

            return null;
        }
    }

    public boolean insert(User registroParaInserir) {
        boolean returningStatus = false;

        try {
            Dao<User,Integer> tpdao =  getDao();

            List<User> users = getAll();
            for (User u:users) {
                tpdao.delete(u);
            }

            tpdao.create(registroParaInserir);
            returningStatus = true;
        } catch (Exception e) {

            e.printStackTrace();
            returningStatus = false;
        }

        return returningStatus;
    }

    public boolean update(User registroParaInserir) {

        boolean returningStatus = false;

        try {
            Dao<User,Integer> tpdao =  getDao();

            User temp = tpdao.queryForId(registroParaInserir.getId());
            if(temp != null){
                tpdao.update(registroParaInserir);
            }else{
                insert(registroParaInserir);
            }
            returningStatus = true;
        } catch (Exception e) {

            e.printStackTrace();
            returningStatus = false;
        }

        return returningStatus;
    }

    @Override
    public void finalize(){
        MasterDAO.fecharDatabase();
    }

}
