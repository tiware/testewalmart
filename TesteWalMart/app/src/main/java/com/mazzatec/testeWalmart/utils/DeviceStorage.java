package com.mazzatec.testeWalmart.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.models.User;

/**
 * Created by Innovative.admin on 11/06/2016.
 */
public class DeviceStorage {
    private static final String preferences = "com.mazzatec.testeWalmart.preferences";

    private static final String preference_user_object = "preference_user_object";
    private static final String preference_reminder_object = "preference_reminder_object";
    private static final String preference_grid = "grid";

    private static String getStringFromPreferences(String key,Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(preferences, Context.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    private static void setStringToPreferences(String key, String value,Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(preferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static User getUserObject(Context context) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(getStringFromPreferences(preference_user_object,context), User.class);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }

    public static void setUserObject(User value,Context context) {
        Gson gson = new Gson();
        setStringToPreferences(preference_user_object, gson.toJson(value), context);
    }

    public static Reminder getReminderObject(Context context) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(getStringFromPreferences(preference_reminder_object,context), Reminder.class);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }

    public static void setReminderObject(Reminder value,Context context) {
        Gson gson = new Gson();
        setStringToPreferences(preference_reminder_object, gson.toJson(value), context);
    }
    public static void setGrid(boolean grid,Context context) {

        String gridString = "false";

        if(grid){

            gridString = "true";

        }

        SharedPreferences sharedPref = context.getSharedPreferences(preferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(preference_grid, gridString);
        editor.commit();
    }


    public static boolean getGrid(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(preferences, Context.MODE_PRIVATE);
        String grid =  sharedPref.getString(preference_grid, "");

        return grid.equals("true");
    }

}
