package com.mazzatec.testeWalmart.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Innovative.admin on 31/03/2016.
 */
@DatabaseTable(tableName = "User")
public class User implements Serializable {

    public User(){

    }

    public User(int id, String nome, String email){

        this.email = email;
        this.nome  = nome;
        this.id = id;

    }

    @DatabaseField(id = true, columnName = "id", canBeNull = false)
    private int id;

    @DatabaseField(canBeNull = true, width = 50, columnName = "nome")
    private String nome;

    @DatabaseField(canBeNull = true, width = 50, columnName = "email")
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
