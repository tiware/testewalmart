package com.mazzatec.testeWalmart.dao;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;


/**
 * Created by Zamariola on 20/10/2015.
 */
public class MasterDAO {

    private static DatabaseHelper sdatabaseHelper;

    public static DatabaseHelper iniciarDataBase(Context contexto){

        if(sdatabaseHelper != null){
            return sdatabaseHelper;
        }else{
            sdatabaseHelper = OpenHelperManager.getHelper(contexto, DatabaseHelper.class);
            return sdatabaseHelper;
        }
    }

    public static void fecharDatabase(){

        if(sdatabaseHelper != null){
            OpenHelperManager.releaseHelper();
            sdatabaseHelper = null;
        }
    }

}
