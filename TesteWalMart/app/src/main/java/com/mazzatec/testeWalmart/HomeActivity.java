package com.mazzatec.testeWalmart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mazzatec.testeWalmart.adapter.ReminderAdapter;
import com.mazzatec.testeWalmart.dao.ReminderDao;
import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.models.TimeAndNoticeMock;
import com.mazzatec.testeWalmart.utils.DateStringUtil;
import com.mazzatec.testeWalmart.utils.DeviceStorage;
import com.mazzatec.testeWalmart.utils.StringUtil;

import java.util.List;

public class HomeActivity extends BaseMenuActivity {

    private List<Reminder> reminderList;

    private GridView grid_reminders;
    private ReminderAdapter reminderAdapter;

    private Context context;

    private boolean gridTwo;

    Reminder currentReminder;

    private EditText editSearch;
    private TextView textTitleCurrent;
    private TextView textDateCurrent;
    private TextView textInfoCurrent;
    private ImageView imgDelete;
    private ImageView imgEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;

        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        editSearch = (EditText) findViewById(R.id.editSearch);
        textTitleCurrent = (TextView) findViewById(R.id.textTitleCurrent);
        textDateCurrent = (TextView) findViewById(R.id.textDateCurrent);
        textInfoCurrent = (TextView) findViewById(R.id.textInfoCurrent);
        imgDelete = (ImageView) findViewById(R.id.imgDelete);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,NewRemminderActivity.class);
                intent.putExtra(StringUtil.CURRENT_REMINDER,currentReminder);
                startActivity(intent);
                finish();
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteReminder(currentReminder);
            }
        });


        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (count < before) {
                    // We're deleting char so we need to reset the adapter data
                    reminderAdapter.resetData();
                }

                reminderAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        gridTwo = DeviceStorage.getGrid(context);

        grid_reminders = (GridView)findViewById(R.id.grid_reminders);
        grid_reminders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //currentReminder = reminderList.get(position);
                currentReminder = (Reminder) grid_reminders.getAdapter().getItem(position);
                updateCurrentReminder();
            }
        });

        ReminderDao reminderDao = new ReminderDao(context);
        reminderList = reminderDao.getAll();

        if(reminderList != null && reminderList.size() > 0){

            currentReminder = reminderList.get(0);

        }else{
            currentReminder = new Reminder();
            currentReminder.setTitle("Você não tem lembretes cadastrados");
        }

        super.setupToolbar(HomeActivity.this);
        super.setupNavigation(HomeActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCurrentReminder();

        if(reminderList != null && reminderList.size() > 0){

            if(gridTwo){
                grid_reminders.setNumColumns(2);
            }else{
                grid_reminders.setNumColumns(1);
            }
            reminderAdapter = new ReminderAdapter(context,reminderList,gridTwo);
            grid_reminders.setAdapter(reminderAdapter);
        }
    }

    private void updateCurrentReminder(){

        if(currentReminder != null){

            textTitleCurrent.setText(currentReminder.getTitle());

            if(currentReminder.getDate() != null){
                textDateCurrent.setText(DateStringUtil.datetoString(currentReminder.getDate()));
            }
            TimeAndNoticeMock timeAndNoticeMock = new TimeAndNoticeMock();
            textInfoCurrent.setText(timeAndNoticeMock.getTimerBefore(currentReminder.getNoticeBefore()));

        }

    }
    private void deleteReminder(Reminder reminder){

        if(reminder != null && reminder.getId() > 0){

            ReminderDao reminderDao = new ReminderDao(context);
            if(reminderDao.delete(reminder)){
                reminderList.remove(reminder);
                reminderAdapter.notifyDataSetChanged();
            }
            if(reminderList != null && reminderList.size() > 0){
                currentReminder = reminderList.get(0);
            }else {
                currentReminder = new Reminder();
            }
            updateCurrentReminder();

        }

    }
}
