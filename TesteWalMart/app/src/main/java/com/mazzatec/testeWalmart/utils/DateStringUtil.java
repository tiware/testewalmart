package com.mazzatec.testeWalmart.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Innovative.admin on 12/06/2016.
 */
public class DateStringUtil {

    private  static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    public static String datetoString(Date date){

        String reportDate = dateFormat.format(date);

        return reportDate;
    }

    public static Date stringToDate(String val) throws ParseException {

        Date date  = dateFormat.parse(val);

        return date;
    }
}
