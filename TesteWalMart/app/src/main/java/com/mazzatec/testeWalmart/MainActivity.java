package com.mazzatec.testeWalmart;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mazzatec.testeWalmart.dao.ReminderDao;
import com.mazzatec.testeWalmart.models.Reminder;
import com.mazzatec.testeWalmart.utils.DeviceStorage;
import com.mazzatec.testeWalmart.utils.StringUtil;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        boolean notification = (PendingIntent.getBroadcast(this, 0, new Intent(StringUtil.NOTIFICATION_STARTED), PendingIntent.FLAG_NO_CREATE) == null);
        if(notification){

            Reminder reminder = DeviceStorage.getReminderObject(context);

            if(reminder != null){

                Intent intent = new Intent(StringUtil.NOTIFICATION_STARTED);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

                long now = System.currentTimeMillis();

                Date dtAlarm = reminder.getDate();

                long alarm = dtAlarm.getTime();
                alarm = alarm - now;

                AlarmManager alarme = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarme.setRepeating(AlarmManager.RTC_WAKEUP, alarm, reminder.getTimeRepeat(), pendingIntent);

            }else{
                Intent intent = new Intent(StringUtil.NOTIFICATION_STARTED);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                AlarmManager alarme = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarme.cancel(pendingIntent);
            }


        }
    }

}
