package com.mazzatec.testeWalmart.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;

import com.mazzatec.testeWalmart.dao.ReminderDao;
import com.mazzatec.testeWalmart.models.Reminder;

/**
 * Created by Innovative.admin on 13/06/2016.
 */
public class InsertNextAlarm {

    public InsertNextAlarm(Context context,Reminder reminderCancel){

        Intent intent = new Intent(StringUtil.NOTIFICATION_STARTED);
        PendingIntent p = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        alarme.cancel(p);

        ReminderDao reminderDao = new ReminderDao(context);

        if(reminderCancel != null){
            reminderCancel.setCancelado(true);
            reminderDao.update(reminderCancel);
        }

        Reminder reminder= reminderDao.getReminder();
        DeviceStorage.setReminderObject(reminder,context);

        if(reminder != null){
            new AlarmUtil(context);
        }


    }
}
